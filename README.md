### Reactive workshop Java starter

Follow [the steps](https://gist.github.com/arkadijs/a2a6901d34272fbbdaa7) and run the starter:

    $ mvn compile exec:java -Dexec.mainClass=instarx.Main

First run will be slow - the dependencies must be downloaded.
