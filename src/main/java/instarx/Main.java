package instarx;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

public class Main {
    String participant = "change-me";
    String clientId = ""; // obtain your own at https://instagram.com/developer
    String api = "https://api.instagram.com/v1";
    String ui = "http://rxdisplay.neueda.lv/in";
    int interval = 5; // seconds
    int count = 10; // count of tagged media to return
    String[] tags = "invent your own".split(" ");
    Scheduler scheduler = Schedulers.newThread();

    public static void main(String[] args) {
        new Main().run();
    }

    // http://unirest.io/java.html
    List<Media> media(String tag) throws UnirestException {
        HttpResponse<JsonNode> resp = Unirest.get *****
            *****
            .asJson();
        return unjson(resp.getBody().getObject()).stream().map(image -> image.setTag(tag)).collect(Collectors.toList());
    }

    void run() {
        // step one: single-threaded HTTP fetch
        Observable<Long> ticker = Observable.*****;
        Observable<String> obstags = Observable.*****(tags);
        Observable<Observable<List<Media>>> nested = ticker.map(_seq ->
            /*Observable<List<Media>>*/ obstags.*****(tag -> {
            return Observable.*****(media(tag)); // and maybe error recovery
        }));
        Observable<List<Media>> list = Observable..*****(nested);

        list.subscribe(images -> /* print */);

        // step two: flatten
        Observable<Media> instagram = Observable.create(/*Subscriber<Media>*/ observer -> {
            list.subscribe(3-args);
        });

        // step three: send to UI
        instagram.subscribe(image -> {
            Unirest.*****(image.setParticipant(participant));
        });

        // step four: deduplicate and verify
        long start = System.currentTimeMillis();
        ConnectableObservable<Media> limited = instagram.takeWhile *****;
        limited.count().forEach *****
        *****


        sleep(100000);
    }

    void sleep(long msec) {
        try { Thread.sleep(msec); } catch (InterruptedException e) { throw new RuntimeException(e); }
    }

    List<Media> unjson(JSONObject json) {
        JSONArray posts = json.getJSONArray("data");
        List<Media> media = new ArrayList<>(posts.length());
        for (int i = 0; i < posts.length(); ++i) {
            JSONObject post = posts.getJSONObject(i);
            JSONObject l = post.optJSONObject("location");
            Location location = l == null ? null : new Location(
                l.optDouble("latitude"), l.optDouble("longitude"), l.has("id") ? l.getInt("id") : null, l.has("name") ? l.optString("name") : null);
            media.add(new Media(post.getJSONObject("images").getJSONObject("thumbnail").getString("url"), location));
        }
        return media;
    }

    public class Location {
        Double latitude; Double longitude; Integer id; String name;
        public Location(Double latitude, Double longitude, Integer id, String name) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.id = id;
            this.name = name;
        }
        @Override
        public String toString() {
            return "Location{" +
                    "latitude=" + latitude +
                    ", longitude=" + longitude +
                    ", id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
        public Double getLatitude() {
            return latitude;
        }
        public Double getLongitude() {
            return longitude;
        }
        public Integer getId() {
            return id;
        }
        public String getName() {
            return name;
        }
    }

    public class Media {
        String tag; String url; Location location; String participant;
        public Media(String url, Location location) {
            this.url = url;
            this.location = location;
        }
        public Media setTag(String tag) {
            this.tag = tag;
            return this;
        }
        public Media setParticipant(String participant) {
            this.participant = participant;
            return this;
        }
        @Override
        public String toString() {
            return "Media{" +
                    "tag='" + tag + '\'' +
                    ", url='" + url + '\'' +
                    ", location=" + location +
                    ", participant='" + participant + '\'' +
                    '}';
        }
        public String getTag() {
            return tag;
        }
        public String getUrl() {
            return url;
        }
        public Location getLocation() {
            return location;
        }
        public String getParticipant() {
            return participant;
        }
    }
}
